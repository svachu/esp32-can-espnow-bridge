#include "espNow.hpp"

ESPNOW ESPNow;
SENSOR_DATA sensorData;
// uint8_t mac[] = {0x5E, 0xCF, 0x7F, 0x27, 0xB6, 0x8C}; // probably vojir
uint8_t mac[] = {0x5E, 0xCF, 0x7F, 0x27, 0xB6, 0x8B}; // dunno maybe hostinec? :D



ESPNOW::ESPNOW(){}

void ESPNOW::init(){
    Serial.print("This node AP mac: "); Serial.println(WiFi.softAPmacAddress());
    Serial.print("This node STA mac: "); Serial.println(WiFi.macAddress());
    Serial.println("> > " + String(sizeof(float)) + " Float size");

    if (esp_now_init()!=0) {
        Serial.println("*** ESP_Now init failed");
        ESP.restart();
    }

    // esp_now_set_self_role(ESP_NOW_ROLE_COMBO); //esp8266
    esp_now_register_recv_cb(OnDataRecv);

    
}

void ESPNOW::loop(){
    
}

void OnDataRecv(const uint8_t *mac, const uint8_t *data, int len) {
    // Serial.print("$$"); // $$ just an indicator that this line is a received ESP-Now message
    // Serial.write(mac, 6); // mac address of remote ESP-Now device
    // Serial.write(len);
    // Serial.write(data, len); 
    // Serial.printf("\r\nReceived\t%d Bytes\t%d", len, data[0]);

    // sensorData;
    // SENSOR_DATA
    // uint8_t bs[sizeof(sensorData)];
    // memcpy(bs, &sensorData, sizeof(sensorData));
    // esp_now_send(NULL, bs, sizeof(sensorData)); // NULL means send to all peers


    

    // Serial.printf("\nSensorDataLen: %d, gotLen: %d\n", sizeof(sensorData), len);
    memcpy((void *)&sensorData, data, sizeof(sensorData)); // that (void *) is impoartant!
    // Serial.println(sensorData.id);
    // Serial.println(sensorData.temp);
    // Serial.println(sensorData.pressure);
    // Serial.println(sensorData.voltage);
    // Serial.println(sensorData.illu);
    // Serial.println(sensorData.humidity);
    
    
    CAN_ID_PROPS canMsg;
    if (sensorData.id[0] == '$') { // this means we have svachu packet! Yay!
        String id = "" + String(sensorData.id[1]) + String(sensorData.id[2]);
        canMsg.sender = (SVACHU_DEVICE) id.toInt();
        Serial.println(id + "> real: " + sensorData.id[1] + sensorData.id[2]);
    } else { // we need to define by hand :(
        // String id = sensorData.id;
        // if (id == "rem_6red")       { canMsg.sender = svachu_device_wireless_weather_outside;
        // } else if (id == "piW_1")   { canMsg.sender = svachu_device_wireless_weather_inside;
        // } else if (id == "piW_2")   { canMsg.sender = svachu_device_wireless_weather_bathroom;
        // } else if (id == "piW_3")   { canMsg.sender = svachu_device_wireless_weather_cellar;
        // } else if (id == "piW_4red")   { canMsg.sender = svachu_device_wireless_weather_bedroom;
        // }
        return;

    }

    canMsg.recipient = svachu_device_svachu_king;
    canMsg.msg_id = svachu_msg_esp_now_msg;
    canMsg.msg_type = svachu_msg_type_sensor;
    canMsg.length = sizeof(float);

    // send pieces
    canMsg.sequence = 0;
    memcpy(canMsg.data, &sensorData.temp, sizeof(float));
    CANBus.send(canMsg);
    
    canMsg.sequence = 1;
    memcpy(canMsg.data, &sensorData.pressure, sizeof(float));
    CANBus.send(canMsg);
    delay(10);

    canMsg.sequence = 2;
    memcpy(canMsg.data, &sensorData.voltage, sizeof(float));
    CANBus.send(canMsg);
    delay(10);

    canMsg.sequence = 3;
    memcpy(canMsg.data, &sensorData.humidity, sizeof(float));
    CANBus.send(canMsg);
    // TODO: send to CAN
}