#ifndef ESP_NOW_HPP_
#define ESP_NOW_HPP_
#include "common.hpp"
#include <esp_now.h>
#include <esp_wifi.h> //NEED THIS TO COMPILE



extern uint8_t mac[];

// keep in sync with ESP_NOW sensor struct
struct __attribute__((packed)) SENSOR_DATA {
  char id[10];
  float temp;
  float pressure;
  float voltage;
  float illu;
  float humidity;
};

extern SENSOR_DATA sensorData;


class ESPNOW {
  public:
    ESPNOW();
    void init();
    void loop();
        
  private:
  

};

void OnDataRecv(const uint8_t *mac, const uint8_t *data, int len);
extern ESPNOW ESPNow;


#endif
