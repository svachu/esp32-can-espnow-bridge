#ifndef CANBUS_HPP_
#define CANBUS_HPP_

#include "common.hpp"
#include <CAN.h>
// library https://github.com/sandeepmistry/arduino-CAN/blob/master/API.md

class CANBUS {
  public:
    CANBUS();
    void init();
    void loop();
    void perSecondLoop();

    void send(CAN_ID_PROPS props);
    void receive();
    
    
    CAN_ID_PROPS lastMessage;
  private:

    uint32_t counter;
    void heartBeat();
    void uptime();

    SVACHU_DEVICE myAddr;
    
  

};

extern CANBUS CANBus;

void can_onReceive(int packetSize);


#endif
