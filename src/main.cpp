#include <Arduino.h>
#include "common.hpp"
#include <ArduinoOTA.h>

// this is called by itself! wow!
void initVariant() {
  WiFi.mode(WIFI_AP);
  WiFi.setHostname("Svachu EspNow");
  esp_wifi_set_mac(ESP_IF_WIFI_AP, &mac[0]);
}


SimpleTimer timer;

void timeree() {
  CANBus.perSecondLoop();
}



void setup() {
  Serial.begin(115200);
  Serial.println("OK, lets boot");
  common_startup();
  timer.setInterval(1000, timeree);

  ESPNow.init();
  CANBus.init();

}

int heartBeat;

void loop() {
  common_loop();
  timer.run();
  CANBus.loop();
  if (millis()-heartBeat > 30000) {
    Serial.println("Waiting for ESP-NOW messages...");
    heartBeat = millis();
  }
}


