#include "canBus.hpp"

CANBUS CANBus;


CANBUS::CANBUS(){
  counter = 0;
  myAddr = svachu_device_esp_repeater;
}

void CANBUS::init(){
  
  if (!CAN.begin(125E3)) { // 125 kbps
    Serial.println("Starting CAN failed!");
    while (1);
  }
  // register the receive callback
  uint32_t filterId = (0b1111111);
  uint32_t filterMask = (0b1111111  & myAddr);
  // TODO: add broadcast
  CAN.filterExtended(filterId, filterMask); // only receive extended with mine

  CAN.onReceive(can_onReceive);
}

void CANBUS::loop(){
  
}

void CANBUS::perSecondLoop(){
  counter++;
  if (counter % 10 == 0) { // every 10 sec heart beat
    heartBeat();
  }

  if (counter % 60*60 == 0) { // every hour send uptime
    uptime();
  }
}

void CANBUS::send(CAN_ID_PROPS props){
  // Serial.print("Sending svachu packet ... ");
  if (props.sequence > 4) {
    Serial.println("Lol sequence is longer than 4! Cant send CAN packet :(");
    return;
  }
  if (props.length > 8) {
		// TODO - lets have some fun!
	} else {
		uint32_t resultAddr = 0;

		resultAddr |= (0b1111111  & props.recipient );
		resultAddr |= (0b1111111  & props.sender ) << 7;
		resultAddr |= (0b1111     & props.msg_type  ) << 14;
		resultAddr |= (0b11111111 & props.msg_id ) << 18;
		resultAddr |= (0b111      & props.sequence   ) << 26;

    Serial.printf("CANPacket> Recipient: %d, Sender: %d, MsgType: %d, MsgId: %d, Seq: %d\n", props.recipient, props.sender, props.msg_type, props.msg_id, props.sequence);
		CAN.beginExtendedPacket(resultAddr);
    CAN.write(props.data, props.length);
    CAN.endPacket();

	}

  Serial.println("done sending");
}

void can_onReceive(int packetSize){
  
  if (!CAN.packetExtended()) {
    Serial.print("Doomed, packet is not extended");
    return;
  }

  if (CAN.packetRtr()) {
    // Remote transmission request, packet contains no data
    Serial.print("RTR, sooo goodbye");
    return;
  }

  Serial.print("packet with id 0x");
  Serial.print(String(CAN.packetId(), HEX));
  Serial.print(" and length ");
  Serial.print(packetSize);
  

  CANBus.lastMessage.length = packetSize;

  uint32_t packetId = CAN.packetId();
	CANBus.lastMessage.recipient  = (SVACHU_DEVICE)   (0b1111111  & packetId);
	CANBus.lastMessage.sender 	  = (SVACHU_DEVICE)   (0b1111111  & (packetId >> 7));
	CANBus.lastMessage.msg_type 	= (SVACHU_MSG_TYPE) (0b1111     & (packetId >> 14));
	CANBus.lastMessage.msg_id 		= (SVACHU_MSG)      (0b11111111 & (packetId >> 18));
	CANBus.lastMessage.sequence 	= 0b111      & (packetId >> 26);

  // only print packet data for non-RTR packets
  int i = 0;
  while (CAN.available()) {
    char piece = (char)CAN.read();
    Serial.println(piece);
    CANBus.lastMessage.data[i] = piece;
    i++;
  }
  

  Serial.println();
  CANBus.receive();
}

void CANBUS::heartBeat(){
  CAN_ID_PROPS msg;
  msg.msg_id = svachu_msg_ping;
  msg.msg_type = svachu_msg_type_info;
  msg.recipient = myAddr;
  msg.sender = myAddr;
  msg.sequence = 0;
  msg.length = 0;
  
  send(msg);
}

void CANBUS::uptime(){
  CAN_ID_PROPS msg;
  msg.msg_id = svachu_msg_uptime;
  msg.msg_type = svachu_msg_type_info;
  msg.recipient = myAddr;
  msg.sender = myAddr;
  msg.sequence = 0;
  msg.length = sizeof(uint32_t);
  uint32_t uptime = millis();
  memcpy(msg.data, &uptime, sizeof(uint32_t));
  
  send(msg);
}

void CANBUS::receive() {

}