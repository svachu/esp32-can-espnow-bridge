#ifndef COMMON_HPP_
#define COMMON_HPP_

#include <SimpleTimer.h>
#include <FS.h>
#include <ESPAsyncWebServer.h>
#include <WebSerial.hpp>
#include <ArduinoJson.h>

#include "credentials.hpp"
#include "can_svachucol.hpp"
#include "canBus.hpp"
#include "espNow.hpp"


void common_startup();
void common_loop();



#endif
